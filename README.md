# Ci Templates

There are 2 types of templates:

## Jobs Template
The first one `foo.template.yml` are only template jobs (ie: jobs name start with a `.`). You can import them, they will not create job or change your pipeline but you can build your own jobs extending them and overiding some values.

```yml
#buzz.template.yml
.job-name-templates
    image: foo
    script:
        - echo bar
```
### Usage
```yml
#.gitlab-ci.yml
include:
  - project: cpelyon/5irc-minekloud/ci-templates
    file: 
      - buzz.template.yml

my-super-duper-job:
    extends: .job-name-templates
```

## Applicable Template
The second one `foo.ci.yml` are real jobs, When you import them you will directly have new jobs in your pipeline. They provide a easy to setup pipeline that work out of the box for most project. Most of the time they internaly use the first kind of template.

### Usage
```yml
#.gitlab-ci.yml
include:
  - project: cpelyon/5irc-minekloud/ci-templates
    file: 
      - buzz.ci.yml
```

warning: if you use multiple of theses template you have to redefine the stages since GitLab can't guess the stages order 

note: You can override some values if you redefine a job with the same name
